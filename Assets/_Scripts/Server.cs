﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Networking;

public class Server : MonoBehaviour
{
	private const int MAX_USER = 100;
	private const int PORT = 26000;
	private const int WEB_PORT = 26001;
	private const int BYTE_SIZE = 1024;

	// a byte is basically an int, but smaller
	private byte reliableChannel;
	private int hostId;
	private int webHostId;

	private bool isStarted = false;
	private byte error;

	private Mongo db;

	#region MonoBehaviour
	void Start()
	{
		DontDestroyOnLoad(gameObject);
		Init();
	}
	void Update()
	{
		UpdateMessagePump();
	}
	#endregion

	public void Init()
	{
		db = new Mongo();
		db.Init();

		NetworkTransport.Init();

		ConnectionConfig cc = new ConnectionConfig();
		reliableChannel = cc.AddChannel(QosType.Reliable);

		HostTopology topo = new HostTopology(cc, MAX_USER);

		// Server only Code
		hostId = NetworkTransport.AddHost(topo, PORT, null);                        // Standalone Server Socket
		webHostId = NetworkTransport.AddWebsocketHost(topo, WEB_PORT, null);        // Webbrowser Server Socket

		Debug.Log($"Opening connection on port {PORT} and webport {WEB_PORT}");
		isStarted = true;
	}

	public void Shutdown()
	{
		isStarted = false;
		NetworkTransport.Shutdown();
	}

	public void UpdateMessagePump()
	{
		if (!isStarted) { return; }

		int recHostId;              // Which platform is sending this?
		int connectionId;           // Which user is sending this? 
		int channelId;              // Which lane is he sending this from?

		byte[] recBuffer = new byte[BYTE_SIZE];         // Array that stores the messages (receiving buffer)
		int dataSize;                                   // Size of the message

		// If we have a message the following information get filled
		NetworkEventType type = NetworkTransport.Receive(out recHostId, out connectionId, out channelId, recBuffer, BYTE_SIZE, out dataSize, out error);
		switch (type)
		{
			case NetworkEventType.Nothing:
				break;

			case NetworkEventType.ConnectEvent:
				Debug.Log($"User {connectionId} has connected through host {recHostId}");
				break;

			case NetworkEventType.DisconnectEvent:
				Debug.Log($"User{connectionId} has disconnected!");
				break;

			case NetworkEventType.DataEvent:
				BinaryFormatter formatter = new BinaryFormatter();
				MemoryStream ms = new MemoryStream(recBuffer);
				NetMsg msg = (NetMsg)formatter.Deserialize(ms);

				OnData(connectionId, channelId, recHostId, msg);
				break;

			default:
			case NetworkEventType.BroadcastEvent:
				Debug.Log("Unexpected network event type");
				break;
		}
	}

	#region OnData
	void OnData(int cnnId, int channelId, int recHostId, NetMsg msg)
	{
		switch (msg.OP)
		{
			case NetOP.None:
				Debug.Log("Unexpected NETOP");
				break;

			case NetOP.CreateAccount:
				CreateAccount(cnnId, channelId, recHostId, (Net_CreateAccount)msg);
				break;

			case NetOP.LoginRequest:
				LoginRequest(cnnId, channelId, recHostId, (Net_LoginRequest)msg);
				break;

			case NetOP.AddFollow:
				AddFollow(cnnId, channelId, recHostId, (Net_AddFollow)msg);
				break;

			case NetOP.RemoveFollow:
				RemoveFollow(cnnId, channelId, recHostId, (Net_RemoveFollow)msg);
				break;

			case NetOP.RequestFollow:
				RequestFollow(cnnId, channelId, recHostId, (Net_RequestFollow)msg);
				break;
		}
	}

	void CreateAccount(int cnnId, int channelId, int recHostId, Net_CreateAccount ca)
	{
		Net_OnCreateAccount oca = new Net_OnCreateAccount();

		if (db.InsertAccount(ca.Username, ca.Password, ca.Email))
		{
			oca.Success = 1;
			oca.Information = "Account was created!";
		}
		else
		{
			oca.Success = 0;
			oca.Information = "There was an error creating the account!";
		}

		SendClient(recHostId, cnnId, oca);
	}
	void LoginRequest(int cnnId, int channelId, int recHostId, Net_LoginRequest lr)
	{
		string randomToken = Utility.GenerateRandom(256);
		Model_Account account = db.LoginAccount(lr.UsernameOrEmail, lr.Password, cnnId, randomToken);
		Net_OnLoginRequest olr = new Net_OnLoginRequest();

		if (account != null)
		{
			olr.Success = 1;
			olr.Information = $"You have been logged in as {account.Username}";
			olr.Username = account.Username;
			olr.Discriminator = account.Discriminator;
			olr.Token = randomToken;
			olr.ConnectionId = cnnId;
		}
		else
		{
			olr.Success = 0;
		}

		SendClient(recHostId, cnnId, olr);
	}
	void AddFollow(int cnnId, int channelId, int recHostId, Net_AddFollow msg)
	{

	}
	void RemoveFollow(int cnnId, int channelId, int recHostId, Net_RemoveFollow msg)
	{
		throw new NotImplementedException();
	}
	void RequestFollow(int cnnId, int channelId, int recHostId, Net_RequestFollow msg)
	{
		throw new NotImplementedException();
	}
	#endregion

	#region Send
	public void SendClient(int recHost, int cnnId, NetMsg msg)
	{
		// This is where we hold our data
		byte[] buffer = new byte[BYTE_SIZE];

		// This is where you would crush your data into a byte[]
		BinaryFormatter formatter = new BinaryFormatter();
		MemoryStream ms = new MemoryStream(buffer);
		formatter.Serialize(ms, msg);

		if (recHost == 0)
			NetworkTransport.Send(hostId, cnnId, reliableChannel, buffer, BYTE_SIZE, out error);
		else
			NetworkTransport.Send(webHostId, cnnId, reliableChannel, buffer, BYTE_SIZE, out error);
	}
	#endregion
}

